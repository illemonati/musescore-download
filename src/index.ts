import jsPDF from "jspdf";

const main = async () => {
    console.log("download");
    const imgsDiv = await getImagesDiv();
    console.log(imgsDiv);
    if (!imgsDiv) return;
    getAllImageUrlsFromDivAndSave(imgsDiv);
};

const getImagesDiv = async (): Promise<HTMLDivElement | undefined> => {
    const images = document.querySelectorAll("img");
    for (const image of images) {
        if (image.src.includes("score_0.")) {
            const div = image.parentElement?.parentElement;
            return div as HTMLDivElement;
        }
    }
};

const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms));

const getAllImageUrlsFromDivAndSave = async (div: HTMLDivElement) => {
    const outImages: HTMLImageElement[] = [];
    const outImageUrls: Set<string> = new Set<string>();
    const outImageDataUrls: string[] = [];
    const divHeight = div.getBoundingClientRect().height;
    const tempCanvas = document.createElement("canvas");
    const ctx = tempCanvas.getContext("2d");
    if (!ctx) return;

    const doc = new jsPDF();
    doc.deletePage(1);
    console.log(divHeight);
    div.scrollTop = 0;
    for (let i = 0; i <= div.scrollHeight; i += divHeight) {
        const images = div.querySelectorAll("img");
        for (const image of images) {
            if (!outImageUrls.has(image.src)) {
                image.crossOrigin = "Anonymous";
                const outImage = document.createElement("img");
                outImage.crossOrigin = "Anonymous";
                outImage.src = image.src;
                await new Promise((r) => (outImage.onload = r));
                console.log(image);
                outImages.push(outImage);
                outImageUrls.add(image.src);
                console.log(3);
                tempCanvas.width = image.width;
                tempCanvas.height = image.height;
                ctx.clearRect(0, 0, tempCanvas.width, tempCanvas.height);
                ctx.drawImage(image, 0, 0, image.width, image.height);
                doc.addPage();
                console.log(tempCanvas.toDataURL());
                doc.addImage(tempCanvas.toDataURL(), 0, 0);
            }
        }
        div.scrollTop = i;
        await sleep(1000);
    }
    doc.save("out.pdf");
};

main();
